# Salesman

A PHP solution for Travelling Salesman Problem using Branch and Bound algorithm.

### Installation

Salesman requires PHP 7.0 and PHPUnit.

Install the dependencies run.

```sh
$ composer install
```