<?php
include_once 'src/ScreenPresenterTsp.php';
include_once 'src/TspService.php';
include_once 'src/FileCityProcessorHelper.php';
include_once 'src/FileHelper.php';
include_once 'src/TspBranchBound.php';

try
{
	$fileHelper = new FileHelper();
	$fileHelper->setFileName("cities.txt");
	$fileHelper->setDelimiter("\t");

	$tspService = new TspService(new FileCityProcessorHelper($fileHelper), new TspBranchBound());
	$result = $tspService->execute();

	ScreenPresenterTsp::printPath($result['path'], $result['locations']);
}
catch (Exception $e)
{
	echo $e;
	exit;
}