<?php
include_once 'src/TspNodeFactory.php';

use PHPUnit\Framework\TestCase; 
final class TspNodeFactoryTest extends TestCase
{
    public function testCreate()
    {
        $level = 1;
        $vertex = 2;
        $tspNode = TspNodeFactory::create([], [], $level, 1, $vertex);
        
        $this->assertInstanceOf(
            TspNode::class,
            $tspNode
        );

        $this->assertAttributeEquals($vertex, 'vertex', $tspNode);
        $this->assertAttributeEquals($level, 'level', $tspNode);
    }
}