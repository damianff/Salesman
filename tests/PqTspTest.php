<?php
include_once 'src/PqTsp.php';
include_once 'src/TspNode.php';

use PHPUnit\Framework\TestCase; 
final class PqTspTest extends TestCase
{
    /**
     * @var PqTsp
     */
    private $pq;
    private $nodeMin;
    private $node;

    public function setUp()
    {
        $this->pq = new PqTsp();

        $this->nodeMin = $this->getMockBuilder(
            TspNode::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->pq->insert($this->nodeMin, 10);

        $this->node = $this->getMockBuilder(
            TspNode::class)
            ->disableOriginalConstructor()
            ->getMock();
       $this->pq->insert($this->node, 20);
    }

    public function testCompare()
    {
        $min = $this->pq->extract();

        $this->assertEquals(
            $min,
            $this->nodeMin
        );
    }
    
}