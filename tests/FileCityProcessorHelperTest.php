<?php
include_once 'src/FileCityProcessorHelper.php';
include_once 'src/FileHelper.php';
include_once 'src/TspLocation.php';

use PHPUnit\Framework\TestCase; 
final class FileCityProcessorHelperTest extends TestCase
{
    private $fileHelper;

    public function setUp()
    {
        $this->fileHelper = $this->getMockBuilder(
            FileHelper::class)
            ->disableOriginalConstructor()
            ->getMock();
        $arrContent = ["Beijing 39.93 116.40", "Tokyo 35.40 139.45", "Vladivostok 43.8 131.54", "Dakar 14.40 -17.28"];
        $this->fileHelper->method('readData')
            ->will($this->returnValue($arrContent));
    }

    public function testCompare()
    {
        $fileCityProcessorHelper = new FileCityProcessorHelper($this->fileHelper);
        $arrLocations = $fileCityProcessorHelper->parse();
        $this->assertEquals(4,count($arrLocations));
        $this->assertInstanceOf(
            TspLocation::class,
            $arrLocations[0]
        );
    }
    
}