<?php
include_once 'src/TspLocation.php';

use PHPUnit\Framework\TestCase; 
final class TspLocationTest extends TestCase
{
    public function testGetInstanceValidRequest()
    {
        $this->assertInstanceOf(
            TspLocation::class,
            TspLocation::getInstance(['latitude' => 1, 'longitude' => 1])
        );
    }

    public function testGetInstanceInvalidRequest()
    {
        $this->expectException(RuntimeException::class);
        TspLocation::getInstance([]);
    }

    public function testDistance()
    {
        $this->assertNotEquals(
            0,
            TspLocation::distance(1,1,1,2)
        );
    }

    public function testDistanceSameLocation()
    {
        $this->assertEquals(
            0,
            TspLocation::distance(1,1,1,1)
        );
    }
}