<?php
include_once 'src/TspBranchBound.php';

use PHPUnit\Framework\TestCase; 
final class TspBranchBoundTest extends TestCase
{
    /**
     * @var TspBranchBound
     */
    private $tsp;

    public function setUp()
    {
        $this->tsp = TspBranchBound::getInstance();
        $this->tsp->addLocation(array('id'=>'newquay', 'latitude'=>50.413608, 'longitude'=>-5.083364));
        $this->tsp->addLocation(array('id'=>'manchester', 'latitude'=>53.480712, 'longitude'=>-2.234377));
        $this->tsp->addLocation(array('id'=>'london', 'latitude'=>51.500152, 'longitude'=>-0.126236));
        $this->tsp->addLocation(array('id'=>'birmingham', 'latitude'=>52.483003, 'longitude'=>-1.893561));
    }

    public function testGetInstance()
    {
        $this->assertInstanceOf(
            TspBranchBound::class,
            TspBranchBound::getInstance()
        );
    }

    public function testAddLocation()
    {
        $tsp = TspBranchBound::getInstance();
        $this->assertTrue(
            $tsp->addLocation(array('id'=>'manchester', 'latitude'=>53.480712, 'longitude'=>-2.234377))
        );
    }

    public function testLoad()
    {
        $locations[] = array('id'=>'newquay', 'latitude'=>50.413608, 'longitude'=>-5.083364);
        $locations[] = array('id'=>'manchester', 'latitude'=>53.480712, 'longitude'=>-2.234377);

        $this->assertTrue(
            TspBranchBound::getInstance()->load($locations)
        );
    }

    public function testLoadEmptyLocations()
    {
        $this->assertFalse(
            TspBranchBound::getInstance()->load([])
        );
    }

    public function testLoadMatrix()
    {
        $this->assertTrue(
            $this->tsp->loadMatrix()
        );
    }

    public function testLoadMatrixEmptyLocations()
    {
        $this->assertFalse(
            TspBranchBound::getInstance()->loadMatrix([])
        );
    }

    public function testSolve()
    {
        $result = $this->tsp->solve();

        $pathCorrect = [[0,2],[2,3],[3,1],[1,0]];
        $this->assertEquals($pathCorrect, $result['path']); 
    }

    public function testSolveEmptyLocations()
    {
        $tsp = TspBranchBound::getInstance();
        $this->assertFalse($tsp->solve()); 
    }
}
