<?php
include_once 'src/interfaces/TspResolverInterface.php';
include_once 'src/interfaces/DataCityProcessorInterface.php';

class TspService
{
    /**
     * var DataCityProcessorInterface
     */
    private $dataCityProcessor;

    /**
     * var TspResolverInterface
     */
    private $tspResolver;

    public function __construct(DataCityProcessorInterface $dataCityProcessor, TspResolverInterface $tspResolver)
    {
        $this->dataCityProcessor = $dataCityProcessor;
        $this->tspResolver = $tspResolver;
    }

    public function execute()
    {
        $locations = $this->dataCityProcessor->parse();

        $this->tspResolver->load($locations);
        return $this->tspResolver->solve();
    }
}