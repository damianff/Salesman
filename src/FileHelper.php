<?php

class FileHelper 
{
	private $fileName;
	private $delimiter;

	public function readData(){ 
			
		 if ((!file_exists($this->fileName)) || (!is_readable($this->fileName))) {
			 throw new RuntimeException('File '.$this->fileName.' not exists');
		 }
	 
		 $id = fopen($this->fileName, "r"); //open the file  

		 while($data = fgetcsv($id, filesize($this->fileName), $this->delimiter)){ 
			 if($data[0]){  
				$arr[] = $data[0];
			 }  
		 }   
		  
		 fclose($id); //close file 
		 return $arr; 
	 }  

	 public function setFileName($fileName)
	 {
		 $this->fileName = $fileName;
 
		 return $this;
	 }
 
	 public function getFileName()
	 {
		 return $this->fileName;
	 }    
	 
	 public function setDelimiter($delimiter)
	 {
		 $this->delimiter = $delimiter;
 
		 return $this;
	 }
 
	 public function getDelimiter()
	 {
		 return $this->delimiter;
	 }

}