<?php

interface TspResolverInterface
{
    public function load($locations);
    public function solve();
}