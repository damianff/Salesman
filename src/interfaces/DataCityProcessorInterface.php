<?php

interface DataCityProcessorInterface
{
    public function parse();
}