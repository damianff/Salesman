<?php
include_once 'src/TspLocation.php';
include_once 'src/FileHelper.php';
include_once 'src/interfaces/DataCityProcessorInterface.php';

class FileCityProcessorHelper implements DataCityProcessorInterface
{
	protected $fileHelper;

	public function __construct($fileHelper)
	{
		$this->fileHelper = $fileHelper;
	}

	public function parse(){ 
		$arrContent = $this->fileHelper->readData();
		$locations = [];
		
        foreach ($arrContent as $line) {
            $arrData = explode(' ',$line);
            $longitude = array_pop($arrData);
            $latitude = array_pop($arrData);
			$name = implode(' ',$arrData);
			if (!empty($longitude) && !empty($latitude)) 
			{
				$locations[] = TspLocation::getInstance(array(
					'id'=>$name,'latitude'=>floatval($latitude),'longitude'=>floatval($longitude)
				));
			}
		}
		return $locations;
	 }  
}