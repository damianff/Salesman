<?php
include_once 'interfaces/PresenterTspInterface.php';

class ScreenPresenterTsp implements PresenterTspInterface
{
	public static function printPath($path, $locations)
	{
		for ($i = 0; $i < count($path); $i++) {
			echo $locations[$path[$i][0]]->id . "\n";
		}
	}
}