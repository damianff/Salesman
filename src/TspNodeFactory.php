<?php
include_once 'src/TspNode.php';

class TspNodeFactory
{

	/**
	 * Constructor
	 *
	 * @param   array    $parentMatrix   The parentMatrix of the costMatrix.
	 * @param   array    $path           An array of integers for the path.
	 * @param   integer  $level          The level of the node.
	 * @param   integer  $i, $j          They are corresponds to visiting city j from city i
	 *
	 */
	public static function create($parentMatrix, $path, $level, $i, $j)
	{	
		$tspNode = new TspNode();

		// stores ancestors edges of state space tree
		$tspNode->path = $path;

		// skip for root node
		if ($level != 0)
			// add current edge to path
			$tspNode->path[] = array($i, $j);

		// copy data from parent node to current node
		$tspNode->reducedMatrix = $parentMatrix;

		// Change all entries of row i and column j to infinity
		// skip for root node
		for ($k = 0; $level != 0 && $k < count($parentMatrix); $k++)
		{
			// set outgoing edges for city i to infinity
			$tspNode->reducedMatrix[$i][$k] = INF;
			// set incoming edges to city j to infinity
			$tspNode->reducedMatrix[$k][$j] = INF;
		}

		// Set (j, 0) to infinity 
		// here start node is 0
		$tspNode->reducedMatrix[$j][0] = INF;

		$tspNode->level = $level;
		$tspNode->vertex = $j;

		return $tspNode;
	}
}